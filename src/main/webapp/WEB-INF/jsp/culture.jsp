<!-- 
 Team: 6
 City: Dallas
 Members:
 		Aksheta Mehta (620522) 	
 		Arun Hariharan Sivasubramaniyan (662528)
 		Bruno de Assis Marques (659338)
 		Jordan Burdett (392025)
 		Yanyi Liang (642967)
-->
 
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Heatmaps</title>

<style>
html,body,#map-canvas {
	width: 100%;
	height: 100%;
	margin: 0px;
	padding: 0px
}

.panel {
	top: 0px;
	left: 50%;
	padding: 5px;
	text-align: right;
}
</style>

<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=visualization"></script>
<script>
	
	
<%String data = (String) request.getAttribute("data");%>
	var map, pointarray, heatmap;
	var json = <%=data%>; 
	
	var jsonData = [];

	for (i = 0; i < json.length; i++) {
		var point = new google.maps.LatLng(json[i].value[1], json[i].value[0]);
		jsonData.push(point);
	}

	function initialize() {
		var mapOptions = {
			zoom : 10,
			center : new google.maps.LatLng(32.785170, -96.799661),
			mapTypeId : google.maps.MapTypeId.SATELLITE
		};

		map = new google.maps.Map(document.getElementById('map-canvas'),
				mapOptions);

		var pointArray = new google.maps.MVCArray(jsonData);

		heatmap = new google.maps.visualization.HeatmapLayer({
			data : pointArray
		});

		heatmap.setOptions({
			dissipating : true,
			maxIntensity : 10,
			radius : 5,
			opacity : 0.9,
		});

		heatmap.setMap(map);
		document.getElementById("total").innerHTML="Number of tweets: " + json.length;
	}

	function toggleHeatmap() {
		heatmap.setMap(heatmap.getMap() ? null : map);
	}

	function changeGradient() {
		var gradient = [ 'rgba(0, 255, 255, 0)', 'rgba(0, 255, 255, 1)',
				'rgba(0, 191, 255, 1)', 'rgba(0, 127, 255, 1)',
				'rgba(0, 63, 255, 1)', 'rgba(0, 0, 255, 1)',
				'rgba(0, 0, 223, 1)', 'rgba(0, 0, 191, 1)',
				'rgba(0, 0, 159, 1)', 'rgba(0, 0, 127, 1)',
				'rgba(63, 0, 91, 1)', 'rgba(127, 0, 63, 1)',
				'rgba(191, 0, 31, 1)', 'rgba(255, 0, 0, 1)' ]
		heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
	}

	function changeRadius() {
		heatmap.set('radius', heatmap.get('radius') ? null : 20);
	}

	function changeOpacity() {
		heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script type="text/javascript">
	function submitform() {
		var form = document.getElementById("myform");
		form.submit();
	}
</script>

<link href="proj.css" rel="stylesheet">

</head>
<body>
	<h1 id="heading">Cultural analysis</h1>
<a href="/GlobalTwittering" class="button2"/>Back to Main</a>

<form id='myform' action="culture">
<table width="100%" style="margin:0px;padding: 0px">
	<tr>
		<td width="40%">
				<h2 class="heading2">Please Select a Language:
				<select id="selectBox" name="lang" onchange="submitform();">
					<option value="" selected>None</option>
					<%
						Set langs = (Set) request.getAttribute("langs");
						for (Iterator iterator = langs.iterator(); iterator.hasNext();) {
							String lang = (String) iterator.next();
							if (lang.equals(request.getAttribute("lang"))) {
					%>
					<option value="<%=lang%>" selected><%=lang%></option>
					<%
						} else {
					%>
					<option value="<%=lang%>"><%=lang%></option>
					<%
						}
						}
					%>
				</select>
		</td>
		<td width="40%">
			<h2>		
				<% if (request.getAttribute("filter").equals("true")) {%>
				<input type="checkbox" name="filter" value="true" checked onchange="submitform();">Only Dallas
				<% } else { %>
				<input type="checkbox" name="filter" value="false" onchange="submitform();">Only Dallas
				<% } %>
			</h2>
		</td>
		<td align="right">
			<h2 id="total"/>
		</td>
	</tr>
</table>
</form>

	<%
		if (data != null && !data.trim().equals("[]")) {
	%>
	<br/>
	<div align="center" id="map-canvas"></div>
	<%
		} else {
	%>
	<h2>No data to display.</h2>

	<%
		}
	%>
</body>
</html>
