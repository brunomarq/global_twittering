<!-- 
 Team: 6
 City: Dallas
 Members:
 		Aksheta Mehta (620522) 	
 		Arun Hariharan Sivasubramaniyan (662528)
 		Bruno de Assis Marques (659338)
 		Jordan Burdett (392025)
 		Yanyi Liang (642967)
-->
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Sentiment</title>
<link href="proj.css" rel="stylesheet">

<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi">
</script>
<script type="text/javascript">

    // Load the Visualization API and the piechart package.
    google.load('visualization', '1.0', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
	function drawChart() {
		<%String data = (String) request.getAttribute("data");%>
    	var json = <%=data%>;
        var negative = [json[2].value, json[0].value, json[1].value, json[3].value];
        var neutral = [json[6].value, json[4].value, json[5].value, json[7].value];
        var positive = [json[10].value, json[8].value, json[9].value, json[11].value];

        var negativetotal = json[0].value+json[1].value+json[2].value+json[3].value;
        var neutraltotal = json[4].value+json[5].value+json[6].value+json[7].value;
        var positivetotal = json[8].value+json[9].value+json[10].value+json[11].value;

        var tr = $('<tr/>');
        tr.append("<td>Negative</td>");
        tr.append("<td>" + negative[0] + "</td>");
        tr.append("<td>" + negative[1] + "</td>");
        tr.append("<td>" + negative[2] + "</td>");
        tr.append("<td>" + negative[3] + "</td>");
        tr.append("<td>" + negativetotal + "</td>");
        $(tr).appendTo('#userdata tbody');
        tr = $('<tr/>');
        tr.append("<td>Neutral</td>");
        tr.append("<td>" + neutral[0] + "</td>");
        tr.append("<td>" + neutral[1] + "</td>");
        tr.append("<td>" + neutral[2] + "</td>");
        tr.append("<td>" + neutral[3] + "</td>");
        tr.append("<td>" + neutraltotal + "</td>");
        $(tr).appendTo('#userdata tbody');
        tr = $('<tr/>');
        tr.append("<td>Positive</td>");
        tr.append("<td>" + positive[0] + "</td>");
        tr.append("<td>" + positive[1] + "</td>");
        tr.append("<td>" + positive[2] + "</td>");
        tr.append("<td>" + positive[3] + "</td>");
        tr.append("<td>" + positivetotal + "</td>");
        $(tr).appendTo('#userdata tbody');
                  

        // Afternoon Chart
       	var afternoon = google.visualization.arrayToDataTable([
  	       	["Mood", "Value"],
          	["Negative", negative[1]],
         	["Neutral", neutral[1]],
         	["Positive", positive[1]]
         	]);
      	var options = {
     			title: 'Afternoon'
      	};

     	var chart = new google.visualization.PieChart(document.getElementById('afternoonchart'));

     	chart.draw(afternoon, options);

        // Morning Chart
        var Morning = google.visualization.arrayToDataTable([
            ["Mood", "Value"],
            ["Negative", negative[0]],
            ["Neutral", neutral[0]],
            ["Positive", positive[0]]
            ]);
        var options = {
                title: 'Morning'
        };

        var chart = new google.visualization.PieChart(document.getElementById('morningchart'));

        chart.draw(Morning, options);

        // Evening Chart
        var Evening = google.visualization.arrayToDataTable([
            ["Mood", "Value"],
            ["Negative", negative[2]],
            ["Neutral", neutral[2]],
            ["Positive", positive[2]]
            ]);
        var options = {
                title: 'Evening'
        };

        var chart = new google.visualization.PieChart(document.getElementById('eveningchart'));

        chart.draw(Evening, options);  

        // Night Chart
        var Night = google.visualization.arrayToDataTable([
            ["Mood", "Value"],
            ["Negative", negative[3]],
            ["Neutral", neutral[3]],
            ["Positive", positive[3]]
            ]);
        var options = {
                title: 'Night'
        };

        var chart = new google.visualization.PieChart(document.getElementById('nightchart'));

        chart.draw(Night, options); 

        // Overall Chart
        var Overall = google.visualization.arrayToDataTable([
            ["Mood", "Value"],
            ["Negative", negativetotal],
            ["Neutral", neutraltotal],
            ["Positive", positivetotal]
            ]);
        var options = {
                title: 'Overall'
        };

        var chart = new google.visualization.PieChart(document.getElementById('overallchart'));

        chart.draw(Overall, options);   

        // Number of Tweets with respect to Mood
        var data1 = google.visualization.arrayToDataTable([
            ["Time of Day", "Morning", "Afternoon", "Evening", "Night"],
            ["Negative", negative[0], negative[1], negative[2], negative[3]],
            ["Neutral", neutral[0], neutral[1], neutral[2], neutral[3]],
            ["Positive", positive[0], positive[1], positive[2], positive[3]]
            ]);

        var options = {
            title : 'Number of Tweets by time of day',
            vAxis: {title: "Number of Tweets"},
            hAxis: {title: "Time of Day"},
            seriesType: "bars"
            };

        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data1, options);

        // Number of Tweets throught day
        var data2 = google.visualization.arrayToDataTable([
            ["Time of Day", "Negative", "Neutral", "Positive"],
            ["Morning", negative[0], neutral[0], positive[0]],
            ["Afternoon", negative[1], neutral[1], positive[1]],
            ["Evening", negative[2], neutral[2], positive[2]],
            ["Night", negative[3], neutral[3], positive[3]]
            ]);

        var options = {
            title: 'Tweets per Time of Day',
            hAxis: {title: 'Time of Day',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
            };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
        chart.draw(data2, options);
                  
    }
</script>
</head>
<body>
	<h2 id="heading">Sentiment Analysis</h2>
    <a href="/GlobalTwittering" class="button2"/>Back to Main</a>
	<div class="wrapper">
	<div class="profile">
   	<table id= "userdata" border="2">
  		<thead>
            <th>Mood</th>
            <th>Morning</th>
            <th>Afternoon</th>
            <th>Evening</th>
            <th>Night</th>
            <th>Overall</th>
        </thead>
     	<tbody>
       </tbody>
   	</table>

	</div>

    <div id="overallchart" style="width: 900px; height: 500px;"></div>
    <table width="100%" cellpadding="0">
        <tr>
            <td id="morningchart" width="25%"/>
            <td id="afternoonchart" width="25%" />
            <td id="eveningchart" width="25%" />
            <td id="nightchart" width="25%" />
        </tr>
    </table>
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
    <div id="chart_div2" style="width: 900px; height: 500px;"></div>

	</div>
</body>
</html>
