<!-- 
 Team: 6
 City: Dallas
 Members:
 		Aksheta Mehta (620522) 	
 		Arun Hariharan Sivasubramaniyan (662528)
 		Bruno de Assis Marques (659338)
 		Jordan Burdett (392025)
 		Yanyi Liang (642967)
-->
<html>
<head>
<link href="proj.css" rel="stylesheet">
</head>
<body>
<h1 id="heading">Global Twittering Views for Dallas</h1>
<table width="100%" cellpadding="50px">
	<tr>
		<td align="center">
			<a href="sentiment" class="button"/>Sentiment Analysis</a>
		</td>
		<td align="center">
			<a href="culture" class="button"/>Cultural Analysis</a>
		</td>
	</tr>
</table>
</body>
</html>