/*
 * Team: 6
 * City: Dallas
 * Members:
 * 		Aksheta Mehta (620522) 	
 * 		Arun Hariharan Sivasubramaniyan (662528)
 * 		Bruno de Assis Marques (659338)
 * 		Jordan Burdett (392025)
 * 		Yanyi Liang (642967)
 * 
 */
package au.edu.unimelb.student.gt.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Java servlet that acts like a controller for preparing and displaying
 * sentiment information.
 *  
 */
public class SentimentController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String lang = req.getParameter("lang");
		String response =
				getCallForRestAPI("http://115.146.95.244:5984/dallas/_design/sentiment/_view/mood_range?group_level=2");

		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(response);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jsonObject = (JSONObject) obj;
		JSONArray jsonArray = (JSONArray) jsonObject.get("rows");

		req.setAttribute("data", jsonArray.toJSONString());

		String nextJSP = "/sentiment.jsp";
		RequestDispatcher dispatcher =
				getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
	}

	private String getCallForRestAPI(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.connect();

		StringBuffer sb = new StringBuffer();

		BufferedReader in =
				new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			sb.append(inputLine);
		}
		in.close();

		return sb.toString();
	}

}
