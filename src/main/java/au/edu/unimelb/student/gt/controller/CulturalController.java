/*
 * Team: 6
 * City: Dallas
 * Members:
 * 		Aksheta Mehta (620522) 	
 * 		Arun Hariharan Sivasubramaniyan (662528)
 * 		Bruno de Assis Marques (659338)
 * 		Jordan Burdett (392025)
 * 		Yanyi Liang (642967)
 * 
 */
package au.edu.unimelb.student.gt.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Java servlet that acts like a controller for preparing and displaying
 * cultural infomartion.
 * 
 * @author Bruno
 * 
 */
public class CulturalController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// Dallas boundaries
	private static double DALLAS_MIN_LAT = -96.977527;
	private static double DALLAS_MAX_LAT = -96.54598;
	private static double DALLAS_MIN_LON = 32.620678;
	private static double DALLAS_MAX_LON = 33.019039;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String lang = req.getParameter("lang");
		String filter = req.getParameter("filter") == null ? "false" : "true";
		req.setAttribute("filter", filter);

		String response =
				getCallForRestAPI("http://115.146.95.244:5984/dallas/_design/languages/_view/langkeys?group_level=1");

		// Parsing the json response for the couchdb rest api call to get
		// language keys
		Set<String> keys = getLanguageKeys(response);
		req.setAttribute("langs", keys);

		if (lang != null) {
			req.setAttribute("lang", lang);
			response =
					getCallForRestAPI("http://115.146.95.244:5984/dallas/_design/languages/_view/lang_with_coords?key=%22"
							+ lang + "%22");
			String coordinates = getCoordinates(response, filter);
			req.setAttribute("data", coordinates);
		}

		String nextJSP = "/culture.jsp";
		RequestDispatcher dispatcher =
				getServletContext().getRequestDispatcher(nextJSP);
		dispatcher.forward(req, resp);

	}

	/**
	 * Gets the coordinates part of the json response from couchdb.
	 * 
	 * @param response
	 * @param filter
	 * @return
	 */
	private String getCoordinates(String response, String filter) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(response);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jsonObject = (JSONObject) obj;
		JSONArray jsonArray = (JSONArray) jsonObject.get("rows");
		JSONArray filteredCoordinates = null;
		if (filter.equals("true")) {
			filteredCoordinates = new JSONArray();
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONArray coords =
						(JSONArray) ((JSONObject) jsonArray.get(i))
								.get("value");
				double lat = Double.parseDouble(coords.get(0).toString());
				double lon = Double.parseDouble(coords.get(1).toString());
				if (lat > DALLAS_MIN_LAT && lat < DALLAS_MAX_LAT
						&& lon > DALLAS_MIN_LON && lon < DALLAS_MAX_LON) {
					filteredCoordinates.add(jsonArray.get(i));
				}

			}
		} else {
			filteredCoordinates = jsonArray;
		}

		return filteredCoordinates.toJSONString();
	}

	/**
	 * Parsing the json response for the couchdb rest api call to get language
	 * keys.
	 * 
	 * @param response
	 * @return
	 */
	private Set<String> getLanguageKeys(String response) {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(response);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		JSONObject jsonObject = (JSONObject) obj;
		JSONArray jsonArray = (JSONArray) jsonObject.get("rows");

		Set<String> keys = new TreeSet<String>();
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject row = (JSONObject) jsonArray.get(i);
			keys.add((String) row.get("key"));
		}
		return keys;
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.doGet(req, resp);
	}

	private String getCallForRestAPI(String urlString) throws IOException {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.connect();

		StringBuffer sb = new StringBuffer();

		BufferedReader in =
				new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {
			sb.append(inputLine);
		}
		in.close();

		return sb.toString();
	}

}
